# $Id$
# Maintainer: Tobias Powalowski <tpowa@archlinux.org>

pkgname=kbd
pkgver=1.15.5
pkgrel=4
pkgdesc="Keytable files and keyboard utilities"
arch=('i686' 'x86_64')
url="http://www.kbd-project.org"
license=('GPL')
depends=('glibc' 'pam')
source=(ftp://ftp.altlinux.org/pub/people/legion/kbd/${pkgname}-${pkgver}.tar.gz
        'fix-keymap-loading-1.15.5.patch'
        'fix-dvorak-es.patch'
        'fix-euro2.patch')
provides=('vlock')
conflicts=('vlock')
replaces=('vlock')

prepare() {
  cd ${srcdir}/${pkgname}-${pkgver}
  # rename keymap files with the same names
  # this is needed because when only name of keymap is specified
  # loadkeys loads the first keymap it can find, which is bad (see FS#13837)
  # this should be removed when upstream adopts the change
  mv data/keymaps/i386/qwertz/cz{,-qwertz}.map
  mv data/keymaps/i386/olpc/es{,-olpc}.map
  mv data/keymaps/i386/olpc/pt{,-olpc}.map
  mv data/keymaps/i386/dvorak/no{,-dvorak}.map
  mv data/keymaps/i386/fgGIod/trf{,-fgGIod}.map
  mv data/keymaps/i386/colemak/{en-latin9,colemak}.map
  # fix https://bugs.archlinux.org/task/33308
  # keymap loading is broken for cetain keymaps
  patch -Np1 -i ../fix-keymap-loading-1.15.5.patch
  # fix es-dvorak #33662
  patch -Np1 -i ../fix-dvorak-es.patch
  # fix euro2 #28213
  patch -Np1 -i ../fix-euro2.patch
}

build() {
  cd ${srcdir}/${pkgname}-${pkgver}
  ./configure --prefix=/usr --datadir=/usr/share/kbd --mandir=/usr/share/man
  make KEYCODES_PROGS=yes RESIZECONS_PROGS=yes
}

package() {
  cd ${srcdir}/${pkgname}-${pkgver}
  make KEYCODES_PROGS=yes RESIZECONS_PROGS=yes DESTDIR=${pkgdir} install
}
md5sums=('34c71feead8ab9c01ec638acea8cd877'
         '4362091d5e23bab2d158f8c7693a45d8'
         '998957c4f815347dcc874c4d7555dc66'
         'd869200acbc0aab6a9cafa43cb140d4e')
