# $Id$
# Maintainer: Andrea Scarpino <andrea@archlinux.org>
# Contributor: Pierre Schmitz <pierre@archlinux.de>

pkgbase=kdenetwork
pkgname=('kdenetwork-filesharing'
         'kdenetwork-kdnssd'
         'kdenetwork-kget'
         'kdenetwork-kopete'
         'kdenetwork-kppp'
         'kdenetwork-krdc'
         'kdenetwork-krfb')
pkgver=4.10.5
pkgrel=2
arch=('i686' 'x86_64')
url='http://www.kde.org'
license=('GPL' 'LGPL' 'FDL')
groups=('kde' 'kdenetwork')
makedepends=('cmake' 'automoc4' 'boost' 'speex' 'libotr3' 'ppp'
	'qca-ossl' 'kdebase-workspace' 'kdebase-lib' 'libvncserver'
    'v4l-utils' 'libidn' 'qimageblitz' 'libxdamage' 'libgadu' 'libmsn'
    'libktorrent' 'libmms' 'telepathy-qt')
source=("http://download.kde.org/stable/${pkgver}/src/${pkgbase}-${pkgver}.tar.xz"
        'use-libotr3.patch'
        'giflib5.patch')
sha1sums=('3ee86a1a227593e9f37881da3c1db1a3c384b2e4'
          '9c3b0ee15538fbfa36aa0a4748b1f6b5a7905384'
          '388319373e96e6f44446ea1132c35ae9f660e01c')

prepare() {
    cd ${pkgbase}-${pkgver}
    patch -p1 -i "${srcdir}"/use-libotr3.patch
    cd kopete
    patch -p1 -i "${srcdir}"/giflib5.patch
}

build() {
	mkdir build
	cd build
	cmake ../${pkgbase}-${pkgver} \
		-DCMAKE_BUILD_TYPE=Release \
		-DKDE4_BUILD_TESTS=OFF \
		-DCMAKE_SKIP_RPATH=ON \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DMOZPLUGIN_INSTALL_DIR=/usr/lib/mozilla/plugins/ \
		-DWITH_Xmms=OFF \
		-DWITH_LibMeanwhile=OFF \
		-DWITH_qq=OFF
	make
}

package_kdenetwork-filesharing() {
	pkgdesc='Konqueror properties dialog plugin to share a directory with the local network'
	depends=('kdelibs' 'smbclient')
	install='kdenetwork.install'
	cd $srcdir/build/kdenetwork-filesharing
	make DESTDIR=$pkgdir install
}

package_kdenetwork-kdnssd() {
	pkgdesc='Monitors the network for DNS-SD services'
	depends=('kdelibs')
	cd $srcdir/build/kdnssd
	make DESTDIR=$pkgdir install
}

package_kdenetwork-kget() {
	pkgdesc='Download Manager'
	depends=('kdebase-workspace' 'kdebase-lib' 'libktorrent' 'libmms')
	url="http://kde.org/applications/internet/kget/"
	install='kdenetwork-kget.install'
	cd $srcdir/build/kget
	make DESTDIR=$pkgdir install
}

package_kdenetwork-kopete() {
	pkgdesc='Instant Messenger'
	depends=('kdebase-runtime' 'kdepimlibs' 'qca-ossl' 'libotr3' 'libmsn'
	         'libidn' 'qimageblitz' 'libgadu')
	url="http://kde.org/applications/internet/kopete/"
	install='kdenetwork-kopete.install'
	cd $srcdir/build/kopete
	make DESTDIR=$pkgdir install
	cd $srcdir/build/kopete/doc
	make DESTDIR=$pkgdir install
}

package_kdenetwork-kppp() {
	pkgdesc='Internet Dial-Up Tool'
	depends=('kdebase-runtime' 'ppp')
	url="http://kde.org/applications/internet/kppp/"
	install='kdenetwork-kppp.install'
	cd $srcdir/build/kppp
	make DESTDIR=$pkgdir install
	cd $srcdir/build/kppp/doc
	make DESTDIR=$pkgdir install
}

package_kdenetwork-krdc() {
	pkgdesc='Remote Desktop Client'
	depends=('kdebase-runtime' 'telepathy-qt')
    optdepends=('libvncserver: VNC support'
                'rdesktop: RDP support'
                'kdebase-keditbookmarks: to edit bookmarks')
	url="http://kde.org/applications/internet/krdc/"
	cd $srcdir/build/krdc
	make DESTDIR=$pkgdir install
	cd $srcdir/build/krdc/doc
	make DESTDIR=$pkgdir install
}

package_kdenetwork-krfb() {
	pkgdesc='Desktop Sharing'
	# note on libxdamage:
	#       not detected by namcap because libgl depends on it
	#       but nvidia providing libgl does not depend on libxdamage
	depends=('kdebase-runtime' 'libvncserver' 'libxdamage' 'telepathy-qt')
	cd $srcdir/build/krfb
	make DESTDIR=$pkgdir install
	cd $srcdir/build/krfb/doc
	make DESTDIR=$pkgdir install
}
