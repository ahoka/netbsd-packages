# $Id$
# Maintainer: Evangelos Foutras <evangelos@foutrelis.com>
# Contributor: Gustavo Alvarez <sl1pkn07@gmail.com>

pkgname=nacl-toolchain-newlib
pkgver=9513
pkgrel=1
_binutilsver=2.20.1
_newlibver=1.20.0
_gccver=4.4.3
pkgdesc="Native Client newlib-based toolchain (only for compiling IRT)"
arch=('i686' 'x86_64')
url="http://code.google.com/chrome/nativeclient/"
license=('BSD' 'GPL3' 'LGPL3' 'GPL2' 'FDL' 'custom')
depends=('zlib' 'mpfr')
options=('!strip')
source=(http://ftp.gnu.org/gnu/binutils/binutils-$_binutilsver.tar.bz2
        ftp://sources.redhat.com/pub/newlib/newlib-$_newlibver.tar.gz
        http://ftp.gnu.org/gnu/gcc/gcc-$_gccver/gcc-$_gccver.tar.bz2
        http://commondatastorage.googleapis.com/nativeclient-archive2/x86_toolchain/r$pkgver/nacltoolchain-buildscripts-r$pkgver.tar.gz
        http://commondatastorage.googleapis.com/nativeclient-archive2/x86_toolchain/r$pkgver/naclbinutils-$_binutilsver-r$pkgver.patch.bz2
        http://commondatastorage.googleapis.com/nativeclient-archive2/x86_toolchain/r$pkgver/naclnewlib-$_newlibver-r$pkgver.patch.bz2
        http://commondatastorage.googleapis.com/nativeclient-archive2/x86_toolchain/r$pkgver/naclgcc-$_gccver-r$pkgver.patch.bz2)
sha256sums=('71d37c96451333c5c0b84b170169fdcb138bbb27397dc06281905d9717c8ed64'
            'c644b2847244278c57bec2ddda69d8fab5a7c767f3b9af69aa7aa3da823ff692'
            '97ed664694b02b4d58ac2cafe443d02a388f9cb3645e7778843b5086a5fec040'
            '3c3d6e63ef8caec24a9e368b72e6c94a28edc8c016da5fef73a312b4a3514909'
            'caaefc2bf90325f152bd0998a29fcad425522a22c6ce373366c4dde2b76c5338'
            'be9e48f8714eaadfc3349ac6d8b077050b719d16a3e7592573670cee7a3c4934'
            'ccbc83627bc7c36f7bbe994ccffe4d51584d8812f2ed62f08528c173e06dcf85')

build() {
  cd "$srcdir"

  mkdir SRC
  mv binutils-$_binutilsver SRC/binutils
  mv newlib-$_newlibver SRC/newlib
  mv gcc-$_gccver SRC/gcc

  for _patch in *.patch; do
    patch -d SRC -Np0 -i "$srcdir/$_patch"
  done

  make PREFIX="$srcdir/$pkgname" CANNED_REVISION="yes" build-with-newlib
}

package() {
  cd "$srcdir"

  install -d "$pkgdir/usr/lib"
  mv $pkgname "$pkgdir/usr/lib/$pkgname"
}

# vim:set ts=2 sw=2 et:
