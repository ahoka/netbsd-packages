# $Id$
# Maintainer: Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor: Ionut Biru <ibiru@archlinux.org>
# Contributor: Hugo Doria <hugo@archlinux.org>

pkgname=deluge
pkgver=1.3.6
pkgrel=1
pkgdesc="A BitTorrent client with multiple user interfaces in a client/server model"
arch=('any')
url="http://deluge-torrent.org/"
license=('GPL3')
depends=(python2-xdg libtorrent-rasterbar twisted python2-pyopenssl
         xdg-utils python2-chardet desktop-file-utils hicolor-icon-theme
         python2-distribute)
makedepends=(intltool pygtk librsvg python2-mako)
optdepends=('python2-notify: libnotify notifications'
            'pygtk: needed for gtk ui'
            'librsvg: needed for gtk ui'
            'python2-mako: needed for web ui')
install=deluge.install
source=(http://download.deluge-torrent.org/source/$pkgname-$pkgver.tar.lzma
        deluged.service deluge-web.service)
md5sums=('956e70928437223d22b4a782134fa8c3'
         '17f51ac9a90f0dc41a20291444a7489b'
         '2a73f62c04e8b147c731fbb50666e846')

build() {
  cd $pkgname-$pkgver
  python2 setup.py build
}

package() {
  cd $pkgname-$pkgver
  python2 setup.py install --prefix=/usr --root="$pkgdir" --optimize=1
  install -Dm644 deluge/data/pixmaps/deluge.svg "$pkgdir/usr/share/pixmaps/deluge.svg"
  mv "$pkgdir/usr/share/icons/scalable" "$pkgdir/usr/share/icons/hicolor/"

  sed -i '1s/python$/&2/' \
    "$pkgdir/usr/lib/python2.7/site-packages/deluge/ui/Win32IconImagePlugin.py"

  cd ..
  install -Dm644 deluged.service "$pkgdir/usr/lib/systemd/system/deluged.service"
  install -Dm644 deluge-web.service "$pkgdir/usr/lib/systemd/system/deluge-web.service"
 
  install -d "$pkgdir/srv"
  install -d -m 775 -o 125 -g 125 "$pkgdir/srv/deluge"
}
