# $Id$
# Maintainer: Jan de Groot <jgc@archlinux.org>
# Contributor: Andrea Scarpino <andrea@archlinux.org>
# Contributor: Thomas Baechler <thomas@archlinux.org>

pkgbase=thunderbird-i18n
pkgver=17.0.7
pkgrel=1
pkgdesc="Language pack for Thunderbird"
arch=('any')
url="http://www.mozilla.com/"
license=('MPL' 'GPL')
depends=("thunderbird>=$pkgver")

_languages=(
  'ar     "Arabic"'
  'ast    "Asturian"'
  'be     "Belarusian"'
  'bg     "Bulgarian"'
  'bn-BD  "Bengali (Bangladesh)"'
  'br     "Breton"'
  'ca     "Catalan"'
  'cs     "Czech"'
  'da     "Danish"'
  'de     "German"'
  'el     "Greek"'
  'en-GB  "English (United Kingdom)"'
  'en-US  "English (United States)"'
  'es-AR  "Spanish (Argentina)"'
  'es-ES  "Spanish (Spain)"'
  'et     "Estonian"'
  'eu     "Basque"'
  'fi     "Finnish"'
  'fr     "French"'
  'fy-NL  "Frisian (Netherlands)"'
  'ga-IE  "Irish"'
  'gd     "Gaelic"'
  'gl     "Galician"'
  'he     "Hebrew"'
  'hr     "Croatian"'
  'hu     "Hungarian"'
  'hy-AM  "Armenian"'
  'id     "Indonesian"'
  'is     "Icelandic"'
  'it     "Italian"'
  'ja     "Japanese"'
  'ko     "Korean"'
  'lt     "Lithuanian"'
  'nb-NO  "Norwegian (Bokmal)"'
  'nl     "Dutch"'
  'nn-NO  "Norwegian (Nynorsk)"'
  'pa-IN  "Punjabi"'
  'pl     "Polish"'
  'pt-BR  "Brazilian Portuguese"'
  'pt-PT  "Portuguese"'
  'rm     "Romansh"'
  'ro     "Romanian"'
  'ru     "Russian"'
  'si     "Sinhalese"'
  'sk     "Slovak"'
  'sl     "Slovenian"'
  'sq     "Albanian"'
  'sr     "Serbian"'
  'sv-SE  "Swedish"'
  'ta-LK  "Tamil (Sri Lanka)"'
  'tr     "Turkish"'
  'uk     "Ukrainian"'
  'vi     "Vietnamese"'
  'zh-CN  "Chinese (simplified)"'
  'zh-TW  "Chinese (traditional)"'
)

pkgname=()
source=()
_url=https://ftp.mozilla.org/pub/mozilla.org/thunderbird/releases/$pkgver/linux-i686/xpi

for _lang in "${_languages[@]}"; do
  _locale=${_lang%% *}
  _pkgname=thunderbird-i18n-${_locale,,}

  pkgname+=($_pkgname)
  source+=("thunderbird-i18n-$pkgver-$_locale.xpi::$_url/$_locale.xpi")
  eval "package_$_pkgname() {
    _package $_lang
  }"
done

# Don't extract anything
noextract=(${source[@]%%::*})

_package() {
  pkgdesc="$2 language pack for Thunderbird"
  install -Dm644 thunderbird-i18n-$pkgver-$1.xpi \
    "$pkgdir/usr/lib/thunderbird/extensions/langpack-$1@thunderbird.mozilla.org.xpi"
}

md5sums=('fb33f8da3bc9f0906baf5222098e9359'
         '0ceada37eed6ea7986fdffd6b0ea5aac'
         '612dcc6e280b547efe9d6cebf82a1eb5'
         'fcc841359ef595fb58e6ef2333453ca0'
         '69c31584fd7b442b655e58e4556f5034'
         '479b6a37772b6885c52f26187818c542'
         'b723a4ae893838f4d332c5118bacd2fb'
         '8cae26bd3354071f96faaa58f633f7f3'
         '99fcd0609b232ba167d22200df617ce4'
         '14e0224981659188f41d15d8a2a116bf'
         '1da89ba6776c5be68851ab6782809b25'
         '4e5e74537f138c0a5cfef02931e57c6b'
         '67bb0b85fd5fd3f7344982cb0f93cc60'
         'a7120022d3a957f152044f5d7ce155e2'
         '44a35e42bc9682b42f7fad416f803b83'
         '149da69e91a018b8c2d2341b62091b18'
         'e78c3749605674623a5ebd455d1da5e6'
         '18d4429db86023c3cff4cdfdb3c7db61'
         '327f085c787a61c95b7904f1d68ba8ba'
         'c7f83c1d5fac8bae0257f5269d6318a9'
         '2d8f1bf1e814fc984d791a95c4ef8811'
         '2513698f14cbc85651c5b752845a9d7f'
         '5d727ff5a17e105abe44b1e970868ea2'
         'e233b53dce6072308c190cdb83cb3d08'
         'f8c1f680d62c76607c12cf517fb4d9b1'
         '124a5110c4e807bae739fa5542b21e6f'
         '8031d7162eeb68343d0879550653c13d'
         'af6f26ef85012c84fb28279230a6bf47'
         '93a25ac664d66deb4e0e88a9dd0f601f'
         'f834580781619b97659dc95d8f203d2b'
         'd54e1e7ed5c5d88db1ef2445bc0c9948'
         '7a899329dafb829075f735990171823f'
         '46fb6882a4350a86616dd0859b230602'
         '80492e996b180062f56f9005d4083608'
         '9fab024f293084e98e00639bb156b7aa'
         'a24adcb523efdfb836d7aa8dcb192a8b'
         '3742697f98cec4bb7c565cdede33263f'
         'd9362ae826eb8591db5d6cf3f56bba0f'
         '5e05ee5c2ef8ee9e5869a1f26277df2c'
         '6cd8b8a4f19df69408ed03e7d09032dd'
         '5ff06adb7f42975034f29910f5c9698d'
         '74b76de767b3d1fc5eae3b40f55bca20'
         'd387e154d9a6d322551a16ac9e9b38e3'
         '03c41d2d47146ca18aab8dff9023d3aa'
         '8fb9aa07d7c5244da75192ebaa254d3c'
         '84377496e3bc15d46f85bef767ca13ba'
         '07ac58fb12e6744279670c22bbe41d9b'
         'b12f1a56bc7ce08c2800955257db1799'
         '36ffa376a0e38e5b7000c2725cca994d'
         '1fb8619dff3c3ee7c07e659a88094b8f'
         '158ca3414b6ac982fc0c97d8b4f28c98'
         '1fdabfde405e2bf54a4c8ad25c04d98f'
         'ffce5a4d36bc8a42496fda405ac677f7'
         'e183f1b3c17813399a930cb7b66d545d'
         '12a93560247c526b0d29cacfc9f4882d')
