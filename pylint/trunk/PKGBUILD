# $Id$
# Maintainer: Stéphane Gaudreault <stephane@archlinux.org>
# Contributor: Alexander Fehr <pizzapunk gmail com>

pkgbase=pylint
pkgname=('python2-pylint' 'python-pylint')
pkgver=0.28.0
pkgrel=1
pkgdesc="Analyzes Python code looking for bugs and signs of poor quality"
arch=('any')
url="http://www.logilab.org/project/pylint"
license=('GPL')
makedepends=('python2-logilab-astng' 'python-logilab-astng' 'python-distribute' 'python2-distribute')
optdepends=('tk: Pylint GUI')
conflicts=('pylint<=0.26.0-2')
source=("http://download.logilab.org/pub/${pkgbase}/${pkgbase}-${pkgver}.tar.gz")
sha1sums=('285b8babd659a9dd21a782f87db0ae15c29728ef')

build() {
  cd "${srcdir}"
  cp -a ${pkgbase}-${pkgver}{,-py3}

  cd ${pkgbase}-${pkgver}

  sed -i "s|/usr/bin/env python|/usr/bin/env python2|" epylint.py
  python2 setup.py build

  cd ../${pkgbase}-${pkgver}-py3

  sed -i "s|/usr/bin/env python|/usr/bin/env python3|" epylint.py
  python3 setup.py build
}

package_python2-pylint() {
  replaces=('pylint<=0.26.0-2')
  depends=('python2-logilab-astng')

  cd "${srcdir}"/${pkgbase}-${pkgver}

  python2 setup.py install --prefix=/usr --root="${pkgdir}" --skip-build --optimize=1

  install -d "${pkgdir}"/usr/share/man/man1
  install -m644 man/* "${pkgdir}/usr/share/man/man1"

  for i in epylint pylint pylint-gui pyreverse symilar; do
     mv "${pkgdir}"/usr/bin/${i}{,2}
     mv "${pkgdir}"/usr/share/man/man1/${i}{,2}.1
  done
}

package_python-pylint() {
  depends=('python-logilab-astng')

  cd "${srcdir}"/${pkgbase}-${pkgver}-py3

  python3 setup.py install --prefix=/usr --root="${pkgdir}" --skip-build --optimize=1

  install -d "${pkgdir}/usr/share/man/man1"
  install -m644 man/* "${pkgdir}/usr/share/man/man1"
}
