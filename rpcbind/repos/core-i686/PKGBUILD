# $Id$
# Maintainer: Tobias Powalowski <tpowa@archlinux.org>

pkgname=rpcbind
pkgver=0.2.0
pkgrel=12
pkgdesc="portmap replacement which supports RPC over various protocols"
arch=(i686 x86_64)
depends=('bash' 'glibc' 'libtirpc')
url="http://rpcbind.sourceforge.net"
license=('custom')
replaces=('portmap')
source=(http://downloads.sourceforge.net/sourceforge/rpcbind/rpcbind-0.2.0.tar.bz2
        rpcbind-sunrpc.patch
        rpcbind.service)
md5sums=('1a77ddb1aaea8099ab19c351eeb26316'
         'c02ac36a98baac70b8a26190524b7b73'
         'a7b23a32be2eb52d7dec52da36d4eba1')

prepare() {
  cd $srcdir/$pkgname-$pkgver
  # patch for iana services file
  patch -Np1 -i ../rpcbind-sunrpc.patch
}

build() {
  cd $srcdir/$pkgname-$pkgver
  ./configure --prefix=/usr --enable-warmstarts --with-statedir=/run
  make
}

check() {
  cd $srcdir/$pkgname-$pkgver
  make check
}

package() {
  cd $srcdir/$pkgname-$pkgver
  make DESTDIR=$pkgdir install
  # install missing man page - https://bugs.archlinux.org/task/21271
  install -m644 man/rpcinfo.8 $pkgdir/usr/share/man/man8/
  # install systemd service file
  install -D -m644 $srcdir/rpcbind.service $pkgdir/usr/lib/systemd/system/rpcbind.service
  # install license
  install -D -m644 COPYING $pkgdir/usr/share/licenses/rpcbind/COPYING
}
