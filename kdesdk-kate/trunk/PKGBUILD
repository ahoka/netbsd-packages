# $Id$
# Maintainer: Andrea Scarpino <andrea@archlinux.org>

pkgbase=kdesdk-kate
pkgname=('kdebase-katepart'
         'kdebase-kwrite'
         'kdesdk-kate')
pkgver=4.10.5
pkgrel=1
arch=('i686' 'x86_64')
license=('GPL' 'LGPL' 'FDL')
makedepends=('kdelibs ''cmake' 'automoc4' 'kdebindings-python')
source=("http://download.kde.org/stable/${pkgver}/src/kate-${pkgver}.tar.xz"
        'pkgbuild-syntax-highlight.patch')
sha1sums=('39cca1dad490ba9ec36b43967afc0159d7343185'
          'dc70306c507083cf9b4bb1c536858742292fa1bf')

prepare() {
  cd kate-${pkgver}
  patch -p1 -i "${srcdir}"/pkgbuild-syntax-highlight.patch
}

build() {
  mkdir build
  cd build
  cmake ../kate-${pkgver} \
    -DCMAKE_BUILD_TYPE=Release \
    -DKDE4_BUILD_TESTS=OFF \
    -DCMAKE_INSTALL_PREFIX=/usr
  make
}

package_kdebase-katepart() {
  pkgdesc="A fast and feature-rich text editor component"
  depends=('kdelibs')
  url="http://kate-editor.org/about-katepart/"
  install='kdebase-katepart.install'

  cd build/part
  make DESTDIR="${pkgdir}" install
}

package_kdebase-kwrite() {
  pkgdesc="Text Editor"
  depends=('kdebase-runtime' 'kdebase-katepart')
  groups=('kde' 'kdebase')
  url="http://www.kde.org/applications/utilities/kwrite/"
  install='kdebase-kwrite.install'

  cd build/kwrite
  make DESTDIR="${pkgdir}" install

  cd ../../build/doc/kwrite
  make DESTDIR="${pkgdir}" install
}

package_kdesdk-kate() {
  pkgdesc="Advanced Text Editor"
  depends=('kdebase-runtime' 'kdebase-katepart' 'qjson')
  groups=('kde' 'kdesdk')
  url="http://www.kde.org/applications/utilities/kate/"
  install='kdesdk-kate.install'
  optdepends=('kdebase-konsole: open a terminal in Kate'
              'kdebindings-python: python bindings')

  cd build/kate
  make DESTDIR="${pkgdir}" install

  cd ../../build/doc/kate
  make DESTDIR="${pkgdir}" install
}
