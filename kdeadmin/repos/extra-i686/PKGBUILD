# $Id$
# Maintainer: Andrea Scarpino <andrea@archlinux.org>
# Contributor: Pierre Schmitz <pierre@archlinux.de>

pkgbase=kdeadmin
pkgname=('kdeadmin-kcron'
         'kdeadmin-ksystemlog'
         'kdeadmin-kuser')
pkgver=4.10.5
pkgrel=1
arch=('i686' 'x86_64')
license=('GPL' 'LGPL' 'FDL')
groups=('kde' 'kdeadmin')
makedepends=('cmake' 'automoc4' 'kdepimlibs')
source=("http://download.kde.org/stable/${pkgver}/src/${pkgbase}-${pkgver}.tar.xz"
        'syslog-path.patch')
sha1sums=('0b809772effe249d1ec581902fcecb73e7761134'
          '20095ce6e0f3e5b6800a7c6e52de6fddba62c031')

prepare() {
	cd ${pkgbase}-${pkgver}
	patch -p1 -i ${srcdir}/syslog-path.patch
}

build() {
	mkdir build
	cd build
	cmake ../${pkgbase}-${pkgver} \
		-DCMAKE_BUILD_TYPE=Release \
		-DKDE4_BUILD_TESTS=OFF \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_strigi-analyzer=OFF
	make
}

package_kdeadmin-kcron() {
	pkgdesc='Configure and schedule tasks'
    url='http://userbase.kde.org/KCron'
	depends=('kdelibs')
	cd $srcdir/build/kcron
	make DESTDIR=$pkgdir install
	cd $srcdir/build/kcron/doc
	make DESTDIR=$pkgdir install
}

package_kdeadmin-ksystemlog() {
	pkgdesc='System log viewer tool'
	depends=('kdebase-runtime')
	url="http://kde.org/applications/system/ksystemlog/"
	cd $srcdir/build/ksystemlog
	make DESTDIR=$pkgdir install
	cd $srcdir/build/ksystemlog/doc
	make DESTDIR=$pkgdir install
}

package_kdeadmin-kuser() {
	pkgdesc='User Manager'
	depends=('kdebase-runtime' 'kdepimlibs')
	url="http://kde.org/applications/system/kuser/"
	install='kdeadmin.install'
	cd $srcdir/build/kuser
	make DESTDIR=$pkgdir install
	cd $srcdir/build/kuser/doc
	make DESTDIR=$pkgdir install
}
